class CreateEletronics < ActiveRecord::Migration
  def change
    create_table :eletronics do |t|
      t.string :name
      t.string :year
      t.string :brand

      t.timestamps null: false
    end
  end
end
