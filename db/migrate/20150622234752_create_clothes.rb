class CreateClothes < ActiveRecord::Migration
  def change
    create_table :clothes do |t|
      t.string :color
      t.string :tissue
      t.string :tamanho

      t.timestamps null: false
    end
  end
end
