class CreateFoundedObjects < ActiveRecord::Migration
  def change
    create_table :founded_objects do |t|
      t.references :tipo, polymorphyc: true, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
