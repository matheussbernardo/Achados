class AddToTable < ActiveRecord::Migration
  def change
	add_column :eletronics, :founded, :boolean
	add_column :eletronics, :lost, :boolean
	
	add_column :clothes, :founded, :boolean
	add_column :clothes, :lost, :boolean
	
	add_column :things, :founded, :boolean
	add_column :things, :lost, :boolean
  end
end
