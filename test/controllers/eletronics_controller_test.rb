require 'test_helper'

class EletronicsControllerTest < ActionController::TestCase
  setup do
    @eletronic = eletronics(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:eletronics)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create eletronic" do
    assert_difference('Eletronic.count') do
      post :create, eletronic: { brand: @eletronic.brand, name: @eletronic.name, year: @eletronic.year }
    end

    assert_redirected_to eletronic_path(assigns(:eletronic))
  end

  test "should show eletronic" do
    get :show, id: @eletronic
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @eletronic
    assert_response :success
  end

  test "should update eletronic" do
    patch :update, id: @eletronic, eletronic: { brand: @eletronic.brand, name: @eletronic.name, year: @eletronic.year }
    assert_redirected_to eletronic_path(assigns(:eletronic))
  end

  test "should destroy eletronic" do
    assert_difference('Eletronic.count', -1) do
      delete :destroy, id: @eletronic
    end

    assert_redirected_to eletronics_path
  end
end
