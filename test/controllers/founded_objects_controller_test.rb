require 'test_helper'

class FoundedObjectsControllerTest < ActionController::TestCase
  setup do
    @founded_object = founded_objects(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:founded_objects)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create founded_object" do
    assert_difference('FoundedObject.count') do
      post :create, founded_object: {  }
    end

    assert_redirected_to founded_object_path(assigns(:founded_object))
  end

  test "should show founded_object" do
    get :show, id: @founded_object
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @founded_object
    assert_response :success
  end

  test "should update founded_object" do
    patch :update, id: @founded_object, founded_object: {  }
    assert_redirected_to founded_object_path(assigns(:founded_object))
  end

  test "should destroy founded_object" do
    assert_difference('FoundedObject.count', -1) do
      delete :destroy, id: @founded_object
    end

    assert_redirected_to founded_objects_path
  end
end
