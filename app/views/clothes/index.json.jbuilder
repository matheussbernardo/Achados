json.array!(@clothes) do |clothe|
  json.extract! clothe, :id, :color, :tissue, :tamanho
  json.url clothe_url(clothe, format: :json)
end
