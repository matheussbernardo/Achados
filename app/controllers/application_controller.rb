class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private
  def is_logged_in?
    unless user_signed_in?
      access_denied
    end
    true
  end

  def access_denied
    @message = "Você precisa estar logado para cadastrar um novo item."

    render :template => 'layouts/_access_denied', :status => 403, :formats => [:html]
  end
end
