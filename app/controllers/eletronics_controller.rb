class EletronicsController < ApplicationController
  before_action :set_eletronic, only: [:show, :edit, :update, :destroy]
  before_filter :is_logged_in?, only: [:edit, :update, :destroy, :new, :create]

  # GET /eletronics
  # GET /eletronics.json
  def index
    @eletronics = Eletronic.all
  end

  # GET /eletronics/1
  # GET /eletronics/1.json
  def show
  end

  # GET /eletronics/new
  def new
    @eletronic = Eletronic.new
  end

  # GET /eletronics/1/edit
  def edit
  end

  # POST /eletronics
  # POST /eletronics.json
  def create
    @eletronic = Eletronic.new(eletronic_params)

    respond_to do |format|
      if @eletronic.save
        format.html { redirect_to @eletronic, notice: 'Eletronic was successfully created.' }
        format.json { render :show, status: :created, location: @eletronic }
      else
        format.html { render :new }
        format.json { render json: @eletronic.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /eletronics/1
  # PATCH/PUT /eletronics/1.json
  def update
    respond_to do |format|
      if @eletronic.update(eletronic_params)
        format.html { redirect_to @eletronic, notice: 'Eletronic was successfully updated.' }
        format.json { render :show, status: :ok, location: @eletronic }
      else
        format.html { render :edit }
        format.json { render json: @eletronic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /eletronics/1
  # DELETE /eletronics/1.json
  def destroy
    @eletronic.destroy
    respond_to do |format|
      format.html { redirect_to eletronics_url, notice: 'Eletronic was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_eletronic
      @eletronic = Eletronic.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def eletronic_params
      params.require(:eletronic).permit(:name, :year, :brand)
    end
end
